<?php

declare(strict_types=1);

namespace Exercises\Reverse;

/**
 * Create methods that reverse given input by its positions.
 *
 * @method static int int(int $number)
 * @method static string string(string $string)
 * @example Reverse::int(12) === 21
 * @example Reverse::int(912) === 219
 * @example Reverse::int(120) === 21
 * @example Reverse::int(-12) === -21
 * @example Reverse::int(-120) === -21
 * @example Reverse::string('qwerty')  === 'ytrewq'
 * @example Reverse::string('apple')  === 'elppa'
 */
final class Reverse
{
    public static function int(int $number)
    {
        $rev_num = 0;

        if ($number < 0) {
            $number = abs($number);
            while ($number > 1) {
                $rev_num = $rev_num * 10 + $number % 10;
                $number = $number / 10;
            }
            $rev_num = '-' . $rev_num;
        }

        while ($number > 1) {
            $rev_num = $rev_num * 10 + $number % 10;
            $number = $number / 10;
        }
        echo $rev_num;
    }

    public static function string(string $string)
    {
        for ($i = mb_strlen($string) - 1; $i >= 0; $i--) {
            echo $string[$i];
        }
    }
}

echo '<br/>';
Reverse::int(12);
echo '<br/>';
Reverse::int(912);
echo '<br/>';
Reverse::int(120);
echo '<br/>';
Reverse::int(-12);
echo '<br/>';
Reverse::int(-120);
echo '<br/>';
Reverse::string('qwerty');
echo '<br/>';
Reverse::string('apple');