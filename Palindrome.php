<?php

declare(strict_types=1);

namespace Exercises\Palindrome;

/**
 * Palindrome is a string that equals itself when reversed.
 * Non-alpha chars must also be included.
 *
 * @method static bool check(string $string)
 * @example Palindrome::check('asddsa')  === true
 * @example Palindrome::check('asdd')  === false
 */
final class Palindrome
{
    public static function check(string $string)
    {
        $firstString = substr($string, 0, strlen($string) / 2);
        $lastString = substr($string, strlen($string) / 2);

        $reverseString = '';
        for ($i = mb_strlen($lastString); $i >= 0; $i--) {
            $reverseString .= $lastString[$i];
        }

        $lastString =  $reverseString;

        if($firstString === $lastString){
            echo 'true';
        }else{
            echo 'false';
        }

    }
}

echo '<br>';
Palindrome::check('asddsa');
echo '<br>';
Palindrome::check('asdd');