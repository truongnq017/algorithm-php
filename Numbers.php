<?php

declare(strict_types=1);

namespace Exercises\Numbers;

/**
 * @method static int add(int $n) add numbers from 1 up to $n including.
 * @example Numbers::add(3) === 6
 */
final class Numbers
{
    private static $sum = 0;

    public static function add(int $n)
    {
        for ($i = 1; $i <= $n; $i++) {
            self::$sum = self::$sum + $i;
        }
        return self::$sum;
    }
}

echo Numbers::add(5);