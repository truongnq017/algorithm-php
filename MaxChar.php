<?php

declare(strict_types=1);

namespace Exercises\MaxChar;

/**
 * Find the most used character.
 *
 * @method static string get(string $string)
 * @example MaxChar::get('qqweqrty')  === 'q'
 * @example MaxChar::get('apple 2202')  === '2'
 */
final class MaxChar
{
    public static function get(string $string)
    {
        for ($i = 0; $i < strlen($string); $i++) {
            echo $string[$i];
            for ($j = 0; $j < $i; $j++) {
                echo $string[$j];
            }
        }
    }
}

MaxChar::get('qqweqrty');